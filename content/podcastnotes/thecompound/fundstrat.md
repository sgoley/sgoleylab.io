https://www.youtube.com/watch?v=VJBt17yBSss

Tom Lee

Biggest surprise of 2021: 
Resilience of the equity market during 2021

Prediction 25% return for S&P based a year of lower realized vol following year of higher realized vol (realized vol for 2020 was 27+)

 As of Sep 30, 2021 Equities represented 24% of household assets

 ![[Pasted image 20220201172239.png]]

Nobody says "I'm overweight equities relative to my house & other assets"

Household Net Worth
![[Pasted image 20220201173244.png]]


Demographic tailwind basically predicts / leads every major bull market.


![[Pasted image 20220201173553.png]]


 Midterm years tend to be backend loaded (seasonally down in first half, up after the midterms)

 Buyback authorization for 2021:
![[Pasted image 20220201174356.png]]

Junk Bonds
![[Pasted image 20220201174732.png]]

Negative real-yields in history
![[Pasted image 20220201175051.png]]

Results of negative real-yields on the equity markets
![[Pasted image 20220201175153.png]]
---
title: Therapy in a Nutshell - Clean vs Dirty Pain
date: 2021-06-29
draft: true
tags: ["therapy", "Explore"]
---

Friend waits two weeks to have coffee and hear a request from a friend.
During that time, person believes it must be about her and comes up with explanations and anxiety about herself.
Instead, friend arrives and says, "I'm getting a divorce."

Summary, are you creating your own suffering?

* How we think about the world
* Distorted Thinking patterns
* Shaming ourselves for having emotions
* Having a victim mindset
* Self-deception
* Avoiding our problems
* Comparison and competition
* Making bad choices
* Chronic Stress

Can't change when you label yourself as "broken"
Unblock yourself from healing

Your own actions, thoughts, or responses can greatly enlarge your pain.

------------------

Takeaways:

Separate on a sheet of paper:

* What you are responsible for
	* The skills I learn
	* How hard I work
	* How I think about problems
	* Where I choose to work
	* Growth mindset
* What you are not responsible for
	* My boss's emotions
	* My boss's assumptions
	* My coworkers
	* The results of the supply chain

------------------

Exercises:

* Use something like the above template to review a recent situation and clearly delineate responsibilities.

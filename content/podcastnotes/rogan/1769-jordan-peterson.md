---
title: Jordan Peterson on The Joe Rogan Experience Ep1769
date: 2022-01-28
draft: false
tags: ["podcasts", "podcastnotes"]
---

#### Surprising Thoughts
 - Addiction tends to be the result of both stress (environmental, emotional, physical) and the availability of an addictive substance. 
* Even freedom is meaningless without some mild form of structure (you are free to move your pieces within the structure of the game of Chess)
* Nihilistism cannot be solved or addressed directly from reason.
* Music is a multi-layered pattern with harmony - effectively that maps to the very fabric of existence. And the meaning is something like: it gives us a window to observe the patterns of being all stacked together and that their is some hope to bringing yourself into an alignment of that.
* Most people are not creative at all according to a scoring.
* Creative is by definition a high risk, high reward strategy
* The future is non-deterministically, non-linearly chaotic and therefore needs some people 
* Gender play is part of the normal variation that allows kids to explore something highly variant for a short time and then close the book. 
* Order of identity creation: Nothing -> Persona -> Shadow -> Anima -> Self
* The shadow can include aggression, sexuality, rejection, etc. whatever it is you that have repressed.
* The bible is the cultural foundation of the West because it is the original book which for some hundreds of years was the only book widely copied & distributed.
* The burden of temporal mortality lifts in the face of genuine dialogue.
* Abraham is the definition of "call to adventure" and "failure to launch"
* What do you set against the suffering of your life? Adventure. Not safety. 
* We each have a bunch of blinding ideas and by stumbling forward together in good faith with an element of judgement which says "yes this and not that"
* What jokes need to be told? What jokes are redemptive to society?
* A good number of interpersonal issues in adulthood are the result of never having learned to play well during childhood. (Not knowing how to take a joke, not knowing how to handle unstructured play)
* Callback (returning to an earlier joke for comedy but generally a way during speaking to give me a check-in for making sure they've been paying attention and giving them a payoff for doing so)
* Kundalini Yoga - awareness practice with some poses
* Reparitive attention being drawn to each part of the "stack of the chakras" - awareness being drawn to all areas of alignment: the atoms, the molecules, the cells, the body, the environment, the world. 
* Attention is capable of being directed towards the various levels of analysis.
* You may not get what you want if you don't tell the truth. But how do you know that you're right in wanting what you think you want anyway? Would not getting what you want be so bad if you actually got something better?
* Ascending the *dominance hierarchy* will make successfuly into the tyrant over Hell. 
* On Twitter, It's just "talk" because it's also written down and public.
* Twitter bot for psychology, addiction, and mental health awareness?
* 
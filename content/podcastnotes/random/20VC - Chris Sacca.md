
https://www.youtube.com/watch?v=VOa7GJyafoM

#### Surprising Thoughts

* Starting from negative (Chris generated a negative $Ms balance in college trading stocks) can be hugely motivating if you don't give into the pressure of it and allow it to swallow you.

* 

 - A lot of successful results from luck but luck isn't always accidental.

 - If or when you are ramping up in life (wealth, lifestyle, relationship, etc.),
sit down with people who have been in that stage and learn about what made them either succeed or fail. Listen, analyze, and if you have a partner - do it together.

 - Couples that work are the ones where both are growing. It can be in different directions but stagnation kills a person internally and you can't have a relationship with an internally dead person.

  - Helping other people out of anxiety / survival modes can significantly improve the environment around you (family, friends, support network)
		  - You may not be aware of how your support network is under-functioning its purpose because of the other members' underlying anxiety / mentality.
  - Power leveling / Achievers produce more human disconnected people. Not everyone should have to compare their life to someone else who is living an accelerated life.
---
title: Chris Sacca on the Tim Ferriss Show
date: 2020-07-05
tags: ["podcasts", "podcastnotes"]
---

<!--- TODO: Finish --->

## Synopsis:

> Chris Sacca is an American venture investor, company advisor, entrepreneur, and lawyer.
> He is the proprietor of Lowercase Capital, a venture capital fund in the United States that has invested in seed and early-stage technology companies such as Twitter, Uber, Instagram, Twilio, and Kickstarter.

## Topics:

-    What is Chris Sacca best known for? [6:10]
-    Total Immersion swimming and how to invest like Chris Sacca [8:40]
-    Pieces of advice given to Chris Sacca on early-stage investing [11:55]
-    What disqualifies startup founders in Sacca’s mind [16:05]
-    Travis Kalanick and Nintendo Wii Tennis [18:55]
-    Traits of founders for whom success, at massive scale, is predestined [22:00]
-    The whales that got away: GoPro, Snapchat, and more [27:40]
-    On letting the negative case dominate one’s analysis for whether to invest or not [29:55]
-    Differences between venture capitalists and private equity [35:15]
-    Books for investing and cultivating emotional intelligence [41:00]
-    Thoughts on modeling what it is to be successful [49:30]
-    What Chris Sacca’s parents did when he was a kid [53:50]
-    What Sacca looks for when hiring [58:25]
-    What historical figure Sacca most identifies with? [1:01:15]
-    Early collecting habits [1:04:25]
-    The story of the prophetic notebook [1:10:10]
-    The two differentiators that shifted the nature of Sacca’s business [1:19:10]
-    Advice to founders or would-be entrepreneurs [1:27:40]
-    Most readily quoted movie [1:31:10]

## Surprising Thoughts:



## Embed:

<iframe src="https://www.art19.com/shows/tim-ferriss-show/episodes/8a26b2f5-7db0-403c-ad49-b15b5c312adc/embed" style="width: 100%; height: 200px; border: 0 none;" scrolling="no”></iframe>

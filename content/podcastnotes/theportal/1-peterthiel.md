---
title: Peter Thiel on The Portal w/ Eric Weinstein
date: 2020-07-05
tags: ["podcasts", "podcastnotes"]
draft: false
---

<!--- TODO: Finish --->

## Synopsis:

> Billionaire Technologist and Investor Peter Thiel is the guest as he joins Eric Weinstein in the studio to Launch "The Portal": a new podcast and video channel dedicated to our search for a path to a more transcendent and transformative future together. Peter and Eric discuss the link between growth and violence and the need to rejoin the quest for a more energizing future for all levels of society.

## Topics:


## Surprising Thoughts:



## Embed:

<iframe src="https://www.art19.com/shows/the-portal/episodes/6740c1c3-c9d2-4ecc-a3df-0ed32f0ddba0/embed" style="width: 100%; height: 200px; border: 0 none;" scrolling="no”></iframe>

---
title: Meta
subtitle: Why do all this? Who is it for?
draft: false
comments: false
---

So far, my motivations with this site can be composed into 2 or 3 summary statements:

1. If you attempt to "learn out loud" there is a chance that people will point out missteps in your thinking.
2. It is a practice and practicing publicly benefits from the transparency that comes with public accountability.
3. I found unpriceable value in the public work of both [Tynan](https://tynan.com/) and [Derek Sivers](https://sive.rs/) and this is my return contribution.

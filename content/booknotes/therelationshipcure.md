---
title: The Relationship Cure
date: 2020-09-14
tags: ["books","book notes","library","growth"]
---

## Synopsis:

> A book that provides a framework for a single unit of emotional connection "the bid".
Also, how to navigate a better life of emotional connection by using that framework in practical, no-fluff examples.

## Topics:

- How We Connect Emotionally
- Look at Your Bids for Connection
- Six Bid Busters and How to Avoid Them
- Discover Your Brian's Emotional Command Systems
- Examine How Your Emotional Heritage sets your style of bidding and therefore your ability to connect.
- Growth your ability to bid and respond to bids intentionally
- Apply / Practice ideas What You've Learned

## Surprising Thoughts:

#### How We Connect Emotionally

- A "bid" is a single unit measure of emotional connection.
<br>
</br>
- Examples of a bid:
     > "Are you busy tonight?"

     > "Can you get me a drink?"

     > "Do you mind if I sit here?"

- Relationships are built through a history of accepted bids over time.
<br>
</br>
- A negative response to a bid communicates no opportunity for emotional connection.
- Research indicates that re-bid (a second bid from the same bidder) after an initial bid is rejected is close to 0% for strangers.
- Bids grow in intensity and frequency as a relationship grows and deepens.
- What is bidding for? For connection. No matter how shallow. Or purely functional. Everyone bids for something.
<br>
</br>
- What are the common responses to a bid? Turn Toward, Turn Against, or Turn Away.
- Responses:
    > Turn Toward = react positively, respond with focused attention on the bidder, ask responsive question, etc.

    > Turn Against = react negatively, respond with sarcasm or ridicule, become argumentative or "being realistic" etc.

    > Turn Away = non-reaction, respond with ignorance of the bid *or* by acting preoccupied.


- Consistent patterns of turning against or turning away among romantic partners leads to elevates occurences of divorce or at least consistent decrease in relationship happiness.
- In normal and stable marriages, an occurences of turning against or turning away can causes spouses to re-bid as infrequently as 20% of the time.
- In marriages headed for divorce, re-bid occurences are nearly as low as between strangers (approaches 0%).

#### Look at Your Bids for Connection

- "Fuzzy bidding" is bidding that self-protects the bidder from outcome disappointment through vagueness. However, chance that rejection can occur through recipient misinterpretation rather than actual rejection.

###### Three responses to a bid

|                   | Turn Toward  | Turn Away    | Turn Against  |
| :------           |:---          | :---         | :---          |
| *How they appear* | Near Passive | Preoccupied  | Belligerent   |
|                   | Low Energy   | Disregarding | Contempt      |
|                   | Attentive    | Interrupting | Contradictory |
|                   | High Energy  | Mindlessness | Critical      |
|                   |              |              | Defensive     |
|                   |              |              | Domineering   |
|                   |              |              |               |
| *How they effect* | More bidding and responding | Less bidding  | Less bidding |
|                   | Growth and development of the relationship  | Loss of confidence | Suppression of feelings |
|                   |              | Relationship ends sooner | Relationship can linger but unhappily |

###### Turning responses to a bid

|Bids for connection| Turn Toward  | Turn Away    | Turn Against  |
| :------           |:---          | :---         | :---          |
| *How they appear* | Near Passive | Preoccupied  | Belligerent   |
|                   | Low Energy   | Disregarding | Contempt      |
|                   | Attentive    | Interrupting | Contradictory |
|                   | High Energy  | Mindlessness | Critical      |
|                   |              |              | Defensive     |
|                   |              |              | Domineering   |
|                   |              |              |               |
| *How they effect* | More bidding and responding | Less bidding  | Less bidding |
|                   | Growth and development of the relationship  | Loss of confidence | Suppression of feelings |
|                   |              | Relationship ends sooner | Relationship can linger but unhappily |


#### Bid Busters

  1. Mindless rather than Mindful

  > By not even noticing emotional bids - or the emotional meaning of the bed - we communicate, at best, unintentional preoccupation.
  > Correction: Be available to bids, don't look for a solution - look for a connection

  2. Starting on a Sour Note

  > By starting out with an accusation or expectation instead of an invitation, we provoke (unintentional or even untrue) a turning against response automatically.
  > Correction: Everything is an invitation, express appreciation, start with "I" insted of "You"

  3. Using Criticism instead of Helpful Criticism

  > A complaint focuses on a specific problem with a behavior. A criticism is a judgement of the person's character and/or assigns blame.
  > Correction: State your need, position from your perceptions, focus on specific behavior instances.

  4. Flooding

  > So stressed or emotionally overwhelmed that clear thinking or productive participation is impossible.
  > Correction: Express your current emotional state and find distance until the soonest possible time to have full emotional participation.

  5. Having a Crabby Habit of Mind

  > Practicing a focused perception of irritability about imperfections. Example from George Carlin about two types of drivers: "Maniacs" who drive faster than him or "Idiots" who drive slower.
  > Correction: Practice another lens of perception - gratitiude, optimism, etc.

  6. Avoidance of Needed Conversations

  > Repressions of interpersonal conflict through healthy mediums (conversation) lead to explosions through normally healthy mediums (fights) or healthy mediums (physical, abusive, etc.)
  > Correction: Strategize to whatever works best for you but **have** the conversation. Scheduled time,


## Amazon Affiliate Link:

<!--- TODO: Fix link --->

<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ac&ref=tf_til&ad_type=product_link&tracking_id=associatejust-20&marketplace=amazon&region=US&placement=0609809539&asins=0609809539&linkId=054217130cace91569bf01ae12a6055d&show_border=false&link_opens_in_new_window=false&price_color=333333&title_color=0066C0&bg_color=FFFFFF"></iframe>

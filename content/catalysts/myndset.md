---
title: Myndset
date: 2021-12-30
draft: false
tags: ["catalyst","music","influence"]
---

<!--- TODO: Finish --->

https://www.mixcloud.com/myndsetmusic/

[A Miraculous Mynd (Myndset Mixtape)](https://www.mixcloud.com/myndsetmusic/a-miraculous-mynd-myndset-mixtape/)

[Dreamers of the Day (Myndset Mixtape)](https://www.mixcloud.com/myndsetmusic/dreamers-of-the-day-myndset-mixtape/)

[To The Stars (Myndset Mixtape)](https://www.mixcloud.com/myndsetmusic/to-the-stars-myndset-mixtape/)

[The Process of Desire (Myndset Mixtape)](https://www.mixcloud.com/myndsetmusic/the-process-of-desire-myndset-mixtape/)

[The Strangest Secret (Myndset Mixtape)](https://www.mixcloud.com/myndsetmusic/the-strangest-secret-myndset-mixtape/)

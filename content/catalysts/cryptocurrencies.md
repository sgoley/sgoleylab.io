---
title: Crypto to me
date: 2021-05-25
draft: false
tags: ["catalyst", "entrepreneur", "lessons", "stories"]
---

At times, people have asked me what sparked my early interest in bitcoin.

I can name at least a few things.

1. [The College Game Day "Bitcoin" kid](https://www.theverge.com/2013/12/1/5163926/one-college-football-sign-netted-22-bitcoins-for-enthusiast)
2. [Jeffrey Tucker's impassioned talk on peer to peer digital economies](https://devoelmoorecenter.com/2015/04/08/fsu-students-learn-about-emerging-sharing-economy/)
3. [Ross Ulbricht & the Silk Road](https://en.wikipedia.org/wiki/Ross_Ulbricht)
4. [Spark Profit](https://web.archive.org/web/20190425140108/https://sparkprofit.com/)

Looking back now, I do see the dots that connected for me. As a broke college student, I never really had the means to invest heavily while the value was still low. But I always knew it would become something. And sometimes feeling _part_ of the journey, even in only a small way, is what makes all the difference.

A few related posts I've put together:

- Crypto Intro

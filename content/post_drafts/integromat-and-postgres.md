---
title: Integromat and PostgreSQL
date: 2019-11-14
draft: true
tags: ["heroku", "postgres","integromat","etl"]
---

## Long time coming

As a very heavy prototyping user of integromat, it genuinely seemed like a major oversight that some of the most popular databases (MySQL, Postgres etc.) had no support except through custom written functions.

However, happy to share an update that I was at least in some way an influencer on the development of:

{{< figure src="/img/integromat/Postgres feature request.png" title="Feature Request comment" >}}

https://www.integromat.com/en/requests/requests-to-update-existing-apps/p/postgresql-more-modules

https://www.integromat.com/en/integrations/postgres

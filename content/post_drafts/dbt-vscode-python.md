---
title: A vscode junkie's guide to dbt and python
date: 2021-08-26
draft: true
tags: ["directives","data","dbt","vscode","python","analytics"]
---


<!--- TODO: Finish --->

------------------

### Step 1: get pyenv-win

LINK: https://github.com/pyenv-win/pyenv-win#get-pyenv-win

My preferred appraoch:
`choco install pyenv-win`

Ensure you can run:

`pyenv --version`

------------------

### Step 2: install preferred versions of python

https://github.com/pyenv-win/pyenv-win#usage

`pyenv install 3.6.8 3.8.10`
etc.

If it's miniconda, you'll have to do a bit more work on the windows version:
https://github.com/pyenv-win/pyenv-win/issues/25


* Download the latest miniconda
* Do the user installation (don't set any variables to PATH)
* Installation directory should be:
    * `C:/Users/MYUSERNAME/.pyenv/pyenv-win/versions/anaconda3-2020.11`

------------------

### Step 3: Set your dbt project to use your pyenv dir

Within your dbt project directory:

`pyenv local 3.8.10` (your version here)

------------------

### Step 3: Create a venv for your dbt project

Per usual with python, create a venv with:

`python -m venv dbt_venv`

Activate it with:
`dbt_venv\Scripts\activate.bat`

> If you already have a requirements txt file:
>
> `pip install -r requirements.txt`

Otherwise:
`pip install dbt==0.20.1` (your version here)

------------------

### Step 4: Set this to be the default python in this vscode workspace:

create .vscode as a directory if it does not exist.
in a file called `settings.json`, drop in:

```
{
    "python.defaultInterpreterPath": "dbt_venv\\Scipts\\python.exe"
}
```

------------------

### Step 5: reload / relaunch and REJOICE

Now when you open this folder, this project, etc, it will always use your pyenv
local python version *and* the specific venv within the project dir.

You can launch terminal (with your venv pre-loaded) any time now by hitting:
`Ctrl+Shift+``

Should anything ever happen to the installation, easily re-clone the repo / dir
and run the following:

```
git clone myrepo.git
cd myrepo
pyenv local 3.8.10
python -m venv dbt_venv
dbt_venv\Scripts\activate.bat
pip install -r requirements.txt
```

And you're back up and running with the original python / dbt environment according to spec.

---
title: The Results Equation
date: 2021-11-21
draft: true
tags: ["self","results","effort"]
---

I was heavily influenced early by the great Earl Nightengale and of course Derek Sivers.
Sources: 
	* [Goal Setting Mastery](https://www.nightingale.com/media/goalsettingguide.pdf)
	* [Ideas are a multiplier on execution](https://sive.rs/multiply)

These sources gave me the groundwork for framing mentalities in either an [action or results]({{< relref "actions-or-results">}}) centric way. 

Which has given me something like the following working version of a formula for results:
*Effort* / *Team* x *Timing* x *Unknown* = Results

Example:

E(100%) / T(Self) * T(Crisis) * U(Family Issues) = Uphill battle

E(70%) / T(2.5) * T(Wave) * U(Remote Wave) = Tailwind to achieve things quickly

It's unfortunate but true. Most people talk about the results that they want but then only input the effort that they *feel* like will be enough. Or get trapped in the dopamine rush of having [shared their goals](https://engine.presearch.org/search?q=dont+tell+people+about+your+goals).

In reality, the opposite is not only more effective but often required. 
Start from the results that you desire, and work backwords to the effort that you will have to generate, the team that you will have to create and incentivize, or simply the environment in which you are attempting to launch your venture.
---
title: Stock Platforms & Trading Tools
draft: false
tags: ["stocks","tradingtools"]
---

* Screening
		- [Wallmine](https://wallmine.com/)
		- [Finviz.com](http://finviz.com/)
		- [Fintel.io](http://fintel.io/)
		- [ShortSqueeze.com](http://shortsqueeze.com/)
		- [HighShortInterest.com](http://highshortinterest.com/)
* Options
		- [Options Profit Calculator](https://www.optionsprofitcalculator.com/)
		- [Barchart](https://www.barchart.com/) (historical options data)
		- [fdscanner.com](http://fdscanner.com/)
		- [SpotGamma](https://spotgamma.com/)
* Charting
		- [TradingView.com](https://www.tradingview.com/)
				- Pinescript language
				- [Pinescript Docs](https://www.tradingview.com/pine-script-docs/en/v4/Introduction.html)
				- [Kodify Intro to Pinescript](https://kodify.net/tradingview-programming-articles/#trading-strategies)
* Data
		- [Alphavantage Data](https://www.alphavantage.co/)
		- [Polygon Data](https://polygon.io/stocks)
		- [Quotient Data](https://rapidapi.com/dubois4and/api/quotient/)
		- [IBorrowDesk - Borrow availability & fee data](https://iborrowdesk.com/)
		- [ShortSqueeze.com - Short Interest data](https://shortsqueeze.com/short_stock_screener.php)
		- [Daily Short Volume Data](https://data.nasdaq.com/data/FINRA-financial-industry-regulatory-authority)
		- [IBKR Data API](https://www.interactivebrokers.com/en/index.php?f=5039)

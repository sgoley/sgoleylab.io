---
title: Template for shppd stack
date: 2019-12-01
draft: false
tags: ["data stack","data", "stack","shppd"]
---

<!--- TODO: Finish --->

## The SHPPD Stack

Learn more about that here

## Setting up the environment

Very helpfully, the SHPPD stack depends on several very capable CLI offered by Heroku, Salesfoce, and DBT. Since the PowerBI report builder is unfortunately still Windows depedent, I find that using Choco is helpful in building this environment.

for the components of this stack, we want to setup the following:

* New salesforce dev environment
* Heroku app shell with Postgres and Connect attachments
* Retrieve Heroku PG default credentials
* Make new DBT repo / instance pointing at PG

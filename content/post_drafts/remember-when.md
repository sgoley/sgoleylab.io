---
title: Social Media - Remember When?
date: 2020-09-01
draft: false
tags: ["psych", "facebook", "social"]
---

#### Social Media

I originally thought that social media was a great thing. I used it for pictures and connections, chat and events. Pretty much like everyone else.

Once I started to work within the data space, I realized some of the implications.

It took me until listening to [Tristan Harris](https://tim.blog/2019/09/19/tristan-harris-2/) to realize how completely designed and intentional the entire system was.

Why *this* kind of experiences had disappeared:

{{< figure src="/img/random/fb_remember_when.png" title="The original News Feed" >}}

As starting points for your own journey, should you like to make it:

I give you the following list of links (Web Archive saved):

* [attention economy](https://archive.is/ljv9e)
* [advertising arbitrage](http://archive.is/WT6v7)
* [high frequency advertising](https://web.archive.org/web/20200217174937/http://www.mqbc.co.uk:80/)

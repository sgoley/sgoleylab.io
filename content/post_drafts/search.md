You may have noticed from a few scattered links to web searches across this site that I don't use Google, Bing, or even DuckDuckGo. 
Instead, I use [Presearch](https://www.presearch.io/). 

If you were wondering why, the answer is very simple and intentional. 

I don't believe there is anything more important than for the primary portal of the greater internet, the search engine, to be 
Why do I use presearch?




https://presearch.org/

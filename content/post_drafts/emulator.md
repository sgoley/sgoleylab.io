---
title: Emulation
date: 2020-09-01
draft: false
tags: ["meaning", "growth","values"]
---

#### Definition

{{< figure src="/img/random/emulation-definition.png" title="What is emulation?" >}}

#### Discussion

Too often, a tradition or perspective is thrown off for the sake of it's condition as "pre-existing".

The fact that something is "pre-existing" is a process of natural selection. I seek to find the sources of greatest selection *success* and emulate them as a starting point.

All of us stand on the shoulders of giants. Even the most creative geniuses were first students of their craft.

* Who would Steve Jobs have been without Edwin Land?
* Would this very page exist without Tim Berners-Lee or Dennis Ritchie?

Unless you have somewhere better to go, first become the best student you can of the best teacher that you can perceive.

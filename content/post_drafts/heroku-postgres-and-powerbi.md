---
title: Heroku Postgres and PowerBi
date: 2019-11-11
draft: false
tags: ["powerbi", "heroku","postgres","data","powerbi"]
---

I've been continuing to receive a very positive response on my custom connector project inspite of the release in August of PowerBi's native support for DirectQuery. The reason being: similar to my own employer [Finexio](https://finexio.com/), there is a large number of startups depending on Heroku as an infrastructure platform.

The primary issue being that when you attempt to make the connection in PowerBI using the native PostgreSQL connector, it depends on a certificate to make the connection secure. While it is possible to issue and manage those certificates for a self managed Postgres instance, it is not possible with all managed services.

Heroku has this to say about their hosted postgres instances:
https://help.heroku.com/3DELT3RK/why-can-t-my-third-party-utility-connect-to-heroku-postgres-with-ssl

As a result, I'm continuing to work on an alternative around this certificate issue but so far it looks as though the ODBC protocol is the only way to circumnaviate this issue for the forseeable future.

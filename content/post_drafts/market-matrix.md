---
title: Recreating my favorite matrix
date: 2018-03-01
draft: true
tags: ["financial data", "shiny","rshiny"]
---

# Table of Contents
1. [The original work](#the-original)
2. [Decisions](#decisions)
3. [Data Sources](#data-sources)
4. [Project Log](#project-log)
5. [Planned Feature Enhancements](#planned-feature-enhancements)

## The original
## Decisions
## Data Sources
## Project Log
## Planned Feature Enhancements

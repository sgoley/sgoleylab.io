---
title: Actions vs Results Mindsets
date: 2020-09-01
draft: false
tags: ["self", "results", "actions"]
---

One of the most important additions to my mental toolset is being able to step back and look at my own approach.

I've come to categorize this in the following way:

`Actions mindset` starts from the perspective

> "I am willing to take these actions."

And subsequently arrives at a particular set of results.

`Results mindset` starts from the persective,

> "I want this particular outcome."

And subsequently works backwards to what actions are necessary.

What is often misaligned is that we can confuse ourselves with positive self-talk about results, when our approach is wholly actions based which can lead to the short cut mentality of,

> "How can I get the outcome I want with only the actions I am wanting to take."

More thoughts to follow on Results

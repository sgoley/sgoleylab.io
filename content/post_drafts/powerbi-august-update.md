---
title: PowerBi August Update
date: 2019-08-20
draft: false
tags: ["powerbi", "customconnector","postgres"]
---

In something of a completely unexpected but very happy twist of events, the PowerBI team has decided to include an update to the native PostgreSQL connector which allows for (Beta) DirectQuery support.

More info here:
https://powerbi.microsoft.com/en-us/blog/power-bi-desktop-august-2019-feature-summary/#postgresql

{{< figure src="/img/powerbi/directquerysupported.png" title="PostgreSQL native integration update" >}}

As a result, I have committed some of my latest changes and published my first "dev" release here:
https://github.com/sgoley/DirectQuery-for-ODBC-in-PowerBI/releases/tag/v0.1-beta

However, as a result of this news, I'll most likely ceasing updating the project in general until I get further updates around the features that will be supported in PowerBI's native connector.

At least in my direct business case, I will still need custom type handling and the ability to set SSL mode which currently is *still* not supported in the native integration even for import mode.

I'm very interested to follow future developments here and will then respond accordingly.

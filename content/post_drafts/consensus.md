---
title: All relationships have consensus algorithms
date: 2022-01-28
draft: false
tags: ["relationships","",""]
---

* each relationship has a consensus algorithm
* that algorithm is can operate along proof of work or proof of stake guidelines
* proof of work = historical chain of competence
* proof of stake = alignment of incentives / skin in the game
* physical fitness is valuable because it demonstrates proof of work (body, work experience, etc.)
* resource staking is valuable because it can function in multiple ways: as a comparison mechanism between potential partners, as a form of incentive testing, and also when the time needed for proof of work is not available.
		* Additional benefit: by raising the cost of commitment, you filter out low commitment & low resource relationship partners. (hedge fund minimums, diamond engagement rings, etc.)
* 
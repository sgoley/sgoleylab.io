---
title: Using PowerApps and Flows to Refresh PowerBI Dataset
date: 2019-03-21
draft: false
tags: ["powerapps", "flow","powerbi"]
---

# Table of Contents

1. [TLDR](#tldr)
2. [Use Case](#use-case)
3. [Microsoft Flow](#microsoft-flow)
4. [Powerapps](#powerapps)
5. [Powerapps Embedded Visual](#powerapps-embedded-visual)
6. [Alignment](#alignment)
7. [Outcomes](#outcomes)


## TLDR

- Made a microsoft flow to trigger powerbi dataset refresh.
- Embedded a powerapp into powerbi report that contains trigger / button for said flow.
- Refresh button for powerbi dataset underlying report? Read more to find out!

## Use Case

I originally started out looking for ways to cause a powerbi dataset refresh from a report.
The standard functionality of powerbi is only to refresh the "view" and not the underlying data.
On the powerbi.com service, you are able to do this in the dataset section of the workspace by hitting a specific button.

{{< figure src="/img/powerapps/2019-05-25 16_28_44-PowerBI-Refresh.png" title="Refresh Now" >}}

This report being an import based model, I needed a way to initiate the dataset refresh from the report.
After an extensive search, I came across a blog post that referenced the possibility of using powerapps to trigger a flow that refreshes the dataset.

There was some attempt to do this before recorded here:
http://dataap.org/blog/2019/02/05/dataset-refresh-in-powerapps-custom-visual/

But I have yet to see any serious writeup of the whole process and its outcomes.
That was enough to get started with!


## Microsoft Flow

First things first, we need to define a callout which will initiate the refresh.

Here is that earlier referenced blog post:
[Refresh PowerBI Dataset with Microsoft Flow](https://medium.com/@Konstantinos_Ioannou/refresh-powerbi-dataset-with-microsoft-flow-73836c727c33)

Big thank you to Konstantinos for that. After completing your refresh callout, you need to substitute the "trigger" with a powerapps input like so:

{{< figure src="/img/powerapps/2019-05-25 16_48_44-Refresh-PowerApp-Flow.png" title="Dataset Refresh Flow" >}}

I spent a good amount of time trying to parameterize the dataset ID so that the same visual could be embedded in any report and operate on the same flow. Something along the lines of:

{{< figure src="/img/powerapps/PowerApp Refresh.png" title="Parameter Flow" >}}

But since I have not been successful in making that version work, I focused on implementing an MVP for a single report using hardcoded dataset values.

## Powerapps

I'm still learning about what powerapps is (first impressions is a better front end for SSIS not unlike how PowerBI is for SSAS) but that said, it's approachable enough to build out an mvp in a day.

[PowerApps](https://powerapps.microsoft.com/en-us/)

As a consequence, I went with the absolute simplest version of an app that I could think of - an entire app dedicated to being a single button, "refresh" - not unlike the target functionality on the dataset refresh button earlier referenced.

I created a 300px wide by 100px tall workspace and built up the visual like so:

{{< figure src="/img/powerapps/2019-05-25 16_48_44-Refresh-PowerApp-Workspace.png" title="Refresh App" >}}

> If you're interested in reusing this app, please contact me directly.

Making sure that you include the PowerBI Integration as well as the elements and making sure at the very least that your button press action is set with the `yourappname.run()` function.

{{< figure src="/img/powerapps/2019-07-27 19_39_42-Demo Dataset Refresh - Button Action.png" title="Button Action Function" >}}

Following that, jump back into microsoft flow and you should be able to setup a process with the trigger being your PowerApp and the triggered action being your Flow callout.

{{< figure src="/img/powerapps/2019-05-25 16_48_44-Refresh-PowerApp-Flow.png" title="Button Action Function" >}}


After setting up that Flow - if you go back to your PowerApp, you should be able to see that PowerApps picked up the interaction here and whenever you "press" your button, it will now trigger your flow to run.

{{< figure src="/img/powerapps/2019-05-25 16_48_44-Refresh-PowerApp-Flow-Association.png" title="Button Action Function" >}}


## Powerapps embedded visual

Last steps for testing - you will need to install the PowerApps Embedded Custom Visual into your PowerBI report much like any other custom visual. Pick that up here:

[PowerApps Visual on AppSource](https://appsource.microsoft.com/en-US/product/power-bi-visuals/wa104381378?tab=overview)

Then, you can login to Powerapps through the visual after you add any data field from your model (You are welcome to try and pass the report id parameter as previously mentioned)

{{< figure src="/img/powerapps/2019-07-27 19_49_39- Demo powerapps test.png" >}}

PowerApps will give you a choice of the apps which are available to you through your login:

{{< figure src="/img/powerapps/2019-07-27 19_49_52-Demo powerapps choose.png" >}}

And most likely embed a large version which is scaled to your visual:

{{< figure src="/img/powerapps/2019-07-27 19_50_23-Demo powerapps Large.png" >}}

So just resize and then start testing to make sure that all the functionality that you built in works as intended!
For this example I included a refresh "spinner" plus shading on hover and a few other UX elements.

{{< figure src="/img/powerapps/2019-07-27 19_50_40-Demo powerapps Resize.png" >}}

## Alignment

One thing that I would like to note in case you develop any similar solutions or build your own with the custom visual is to pay attention to the alignment issue. There are some super helpful settings within the App Settings below which you will definitely need to tune to the version you are emedding. I had numerous issues before finding the scaling and orientation toggles.

{{< figure src="/img/powerapps/2019-05-25 16_48_44-Refresh-PowerApp-Scaling.png" title="Scaling" >}}

## Outcomes

- The Flow worked as intended but could not pass a parameter meaning that each flow and therefore each button would have to be tied to a single dataset
- While the underlying dataset did refresh, this did not effect the current session (visuals do not refresh in real time)
- There is no way of getting progress on the refresh! From a design and interactivity perspective, it seemed to result in a very quick "refresh" time since the visual performed the refresh action of initiating the dataset refresh quickly but received no feedback on the dataset refresh progress. Ideally, the visual would continue with the "in progress" visual until the whole flow has been confirmed as complete.
